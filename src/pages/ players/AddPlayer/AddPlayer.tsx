import React, { useEffect, useState } from "react";
import Select from "react-select";
import { useDispatch, useSelector } from "react-redux";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router";
// components
import {
  InputStyleWidthSmall,
} from "../../../ui/styledComponents/inputs/InputStyled";
import { multiSelectStyles } from "../../../ui/styledComponents/Multiselects/multiSelectStyles";
import {
  CommonHeader,
  CommonTitle,
  CommonInputImage,
  CommonInput,
  CommonButton,
} from "../../../common/components/CommonPlayerAndTeamComponent/AddTeamAndAddPlayerComponent";
// sttyles
import "./AddPlayer.css";
import "../../../ui/commonStyleClasses/commonStyle.css";
// interface
import { InputsAddPlayer } from "../../../common/components/interfaces/intarfaces";
// redux
import {
  addNewPlayer,
  getPositions,
} from "../../../core/redux/players/playerCreateSlice";

let valuesSelect = [
  { label: "Center Forward", value: "react" },
  { label: "Denver Nuggets", value: "denver" },
];

const AddPlayer = () => {
  const dispatch = useDispatch();
  const selectorPositions = useSelector((state) => state.players.positions);
  const [playerData, setPlayerData] = useState(null);
  const redirect = useNavigate();

  const {
    register,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm<InputsAddPlayer>();

  const onSubmit: SubmitHandler<InputsAddPlayer> = (data: InputsAddPlayer) => {
    dispatch(addNewPlayer({

        "name": "string",
        "number": 0,
        "position": "string",
        "team": 0,
        "birthday": "2022-05-30T15:15:27.686Z",
        "height": 0,
        "weight": 0,
        "avatarUrl": "string"

    }))
    setPlayerData(data);
  };
  //console.log(playerData);

  const positions = selectorPositions.map((el, idx) => {
    return { label: el, value: idx };
  });

  const selectorGetPlayer = useSelector(state => state.players.getPlayer)
  const selectorIsUpdatePlayerStatus = useSelector(state => state.players.isUpdatePlayerStatus)

  const {id, name, division, foundationYear, imageUrl, conference} = selectorGetPlayer

  useEffect(() => {
    dispatch(getPositions());
  }, []);

  const cancelAddDataPlayer = () => {
    setPlayerData("");
    reset({
      name: "",
      width: "",
      height: "",
      birthday: "",
      avatar: "",
      number: "",
    });
    redirect("/players");
  };

  return (
    <section className="addplayer container">
      <CommonHeader headerTitle="AddPlayer" />
      <div className="addplayer-container">
        <CommonTitle title="Players" name="Greg Whittington" />
        <div className="addplayer__profile">
          <CommonInputImage 
          selectorisUpdateStatus={selectorIsUpdatePlayerStatus}
          valueImage={imageUrl}
          register={register} 
          name="imageUrl"
          />
          

          <CommonInput 
          selectorisUpdateStatus={selectorIsUpdatePlayerStatus}
          value={name}
          name="division"
          register={register} label="Name" />

          <div className="profile__input-position common-input">
            <label htmlFor="position" className="common-text-style">
              Position
            </label>
            <div className="input-position__multiselect">
              <Select
                styles={multiSelectStyles}
                isMulti
                options={positions}
                defaultValue={positions[2]}
              />
            </div>
          </div>

          <div className="profile__input-team common-input">
            <label htmlFor="position" className="common-text-style">
              Team
            </label>
            <div className="input-team__multiselect">
              <Select
                styles={multiSelectStyles}
                isMulti
                options={valuesSelect}
                defaultValue={valuesSelect[1]}
              />
            </div>
          </div>

          <div className="profile__input-data common-input-data">
            <div className="common-margin-left">
              <label htmlFor="weight" className="common-text-style">
                Weight (kg)
              </label>
              <InputStyleWidthSmall
                {...register("width", { maxLength: 20, required: true })}
                id="weight"
              />
            </div>
            <div>
              <label htmlFor="height" className="common-text-style">
                Height (cm)
              </label>
              <InputStyleWidthSmall
                {...register("height", { maxLength: 20, required: true })}
                id="height"
              />
            </div>
          </div>

          <div className="profile__input-data common-input-data">
            <div className="common-margin-left input-data__birthday">
              <label htmlFor="Birthday" className="common-text-style">
                Birthday
              </label>
              <InputStyleWidthSmall
                {...register("birthday", { maxLength: 20, required: true })}
                id="Birthday"
              />
              <div className="calendar-icon"></div>
            </div>
            <div>
              <label htmlFor="Number" className="common-text-style">
                Number
              </label>
              <InputStyleWidthSmall
                {...register("number", { maxLength: 20, required: true })}
                id="Number"
              />
            </div>
          </div>

          <CommonButton
            cancelAddDataPlayer={cancelAddDataPlayer}
            handleSubmit={handleSubmit}
            onSubmit={onSubmit}
          />
        </div>
      </div>
    </section>
  );
};

export default AddPlayer;
