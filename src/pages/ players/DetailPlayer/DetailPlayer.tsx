import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
// components
import { CommonlHeader } from "../../../common/components/CommonPlayerAndTeamComponent/DetailsTeamAdnPlayersComponent";
import { CommonHeaderName } from "../../../common/components/CommonPlayerAndTeamComponent/DetailsTeamAdnPlayersComponent";
// styles
import "./DetailPlayer.css";
import "../../../ui/commonStyleClasses/commonStyle.css";
// redux
import { changeUpdatePlayerStatus, deletePlayer } from "../../../core/redux/players/playerCreateSlice";

const DetailPlayer = () => {
  const dispatch = useDispatch()
  const selectorGetPlayer = useSelector(state => state.players.getPlayer)
  const redirect = useNavigate()
  const {id, name, foundationYear, division, imageUrl, conference} = selectorGetPlayer

  const changeStatus = () => {
    dispatch(changeUpdatePlayerStatus(true))
    redirect('/addplayer')
    console.log('updatedstatus');
    
  }

  const handleDelete = () => {
    dispatch(deletePlayer(id))
    dispatch(changeUpdatePlayerStatus(false))    
    redirect('/addplayer')
  }

  return (
    <section className="detail-player container">
      <CommonlHeader 
      text="players" />
      <main className="card-player">
        <CommonHeaderName 
         changeStatus={changeStatus}
         handleDelete={handleDelete}
        title="Players" name="Greg Whittington" />
        <div className="card-player__player-profile">
          <div className="player-profile__image"></div>
          <span className="player-profile__name common-header common-text-style">
            Greg Whittington <span className="name__number">#22</span>
          </span>
          <div className="player-profile__position common-flex">
            <span className="position__position common-header common-text-style">
              Position
            </span>
            <span className="position__forward common-text common-text-style">
              Forward
            </span>
          </div>
          <div className="player-profile__team common-flex">
            <span className="team__team common-header common-text-style">
              Team
            </span>
            <span className="team__title common-text common-text-style">
              Denver Nuggets
            </span>
          </div>
          <div className="player-profile__height common-flex">
            <span className="height__height common-header common-text-style">
              Height
            </span>
            <span className="height__size common-text common-text-style">
              206 cm
            </span>
          </div>
          <div className="player-profile__weight common-flex">
            <span className="height__weight common-header common-text-style">
              Weight
            </span>
            <span className="height__size common-text common-text-style">
              95 kg
            </span>
          </div>
          <div className="player-profile__age common-flex">
            <span className="height__age common-header common-text-style">
              Age
            </span>
            <span className="height__size common-text common-text-style">
              27
            </span>
          </div>
          <div className="player-profile__footer"></div>
        </div>
      </main>
    </section>
  );
};

export default DetailPlayer;
