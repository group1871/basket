import React, { useState, useEffect } from "react";
import Select from "react-select";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
// components
import {
  EmptyContainer,
  InputSearch,
  Button,
  PaginationContainer,
} from "../../../common/components/CommonPlayerAndTeamComponent/PlayerAndTeamComponent";
import Header from "../../../common/components/PlayersHeader";
// request
import { get } from "../../../api/request";
// styles
import "../../../ui/commonStyleClasses/commonStyle.css";
import "./Players.css";
import { SelectPaginationStyles } from "../../../ui/styledComponents/Multiselects/SelectPaginationStyles";
import { multiSelectStyles } from "../../../ui/styledComponents/Multiselects/multiSelectStyles";
// redux
import { fetchPlayers,changeAddPlayerStatus, getPlayer, changeCountSelectPaginate } from "../../../core/redux/players/playerCreateSlice";

const options = [
  { label: "angular", value: "angular js" },
  { label: "react", value: "react js" },
];

const optionsPaginate = [
  { label: "6", value: "6" },
  { label: "12", value: "12" },
  { label: "24", value: "24" },
];

const items = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14];

const Players = () => {
  // const [addPlayer, setAddPlayer] = useState(false)
  const redirect = useNavigate();
  const dispatch = useDispatch();
  const playersData = useSelector((state) => state.players.players);
  const {name, count} = playersData

  const selectorAddPlayerStatus = useSelector(
    (state) => state.players.isAddTeamStatus
  );

  const selectorCountSelectPaginate = useSelector(state => state.players.countSelectPaginate)



  function addNewPlayer() {
    //setAddPlayer(true)
    redirect("/addplayer");
  }

  const changeCountSelect = (value) => {
    dispatch(changeCountSelectPaginate(value))
  }

  useEffect(() => {
    dispatch(fetchPlayers())
    if(count >= 1) {
      dispatch(changeAddPlayerStatus(true))
    }
  }, [])
  console.log(playersData);

 

//   useEffect(() => {

   
//     dispatch(getPlayers());

//    if(count >= 1) {
//     dispatch(changeAddPlayerStatus(true))
//  }
  
  return (
    <section className="players container">
      <h2 className="players__h2">Players</h2>
      <Header />

      <div className="players__search">
        <InputSearch />
        <div className="players__multiselect">
          <Select
            styles={multiSelectStyles}
            isMulti
            defaultValue={options[1]}
            menuShouldScrollIntoView
            options={options}
          />
        </div>

        <Button addNewPlayer={addNewPlayer} />
      </div>
      <EmptyContainer 
      selectorAddStatus={selectorAddPlayerStatus}

      text="players" />

      <PaginationContainer
        optionsPaginate={optionsPaginate}
        SelectPaginationStyles={SelectPaginationStyles}
        defaultValue={optionsPaginate[0]}
        selectorCountSelectPaginate={selectorCountSelectPaginate}
        changeCountSelect={changeCountSelect}
      />
    </section>
  );
};

export default Players;
