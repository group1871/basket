import React from "react";
import { Link } from "react-router-dom";
// components
import FormRegistration from "../../../common/components/Form/FormRegistration";
// styles
import "../../../ui/commonStyleClasses/commonStyle.css";
import "./Registration.css";

const Registration = () => {
  return (
    <section className="registration container">
      <h2 className="header common-text-style">Sign Up</h2>
      <FormRegistration />
      <div className="footer-text">
        <span className="footer-text__link-to-login common-text-style">
          Already a member?
          <Link
            style={{ textDecoration: "none", color: "red", paddingLeft: "3px" }}
            to="/"
          >
            Sign in
          </Link>
        </span>
      </div>
    </section>
  );
};

export default Registration;
