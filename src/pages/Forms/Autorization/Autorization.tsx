import React from "react";

import { Link } from "react-router-dom";
// components
import FormAutorization from "../../../common/components/Form/FormAutorization";
// styles
import "../Registration/Registration.css";
import "../../../ui/commonStyleClasses/commonStyle.css";

const Autorization = () => {
  return (
    <section className="container">
      <h2 className="header common-text-style">Sign In</h2>
      <FormAutorization />
      <div className="footer-text">
        <span className="footer-text__link-to-login common-text-style">
          Not a member yet?
          <Link
            style={{ textDecoration: "none", color: "red", paddingLeft: "3px" }}
            to="/registration"
          >
            Sign up
          </Link>
        </span>
      </div>
    </section>
  );
};

export default Autorization;
