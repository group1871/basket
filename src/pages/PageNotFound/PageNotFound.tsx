import React from "react";
// styles
import "../../ui/commonStyleClasses/commonStyle.css";
import "./PageNotFound.css";

const PageNotFound = () => {
  return (
    <section className="page-not-found container">
      <h2 className="page-not-found__header">Page Not Found</h2>
      <div className="page-not-found__box">
        <div className="box__image"></div>
        <span className="box__title-not-found common-text-style">
          Page not found
        </span>
        <span className="box__message-not-found common-text-style">
          Sorry, we can’t find what you’re looking for
        </span>
      </div>
    </section>
  );
};

export default PageNotFound;
