import React  from "react";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";
// components
import { CommonlHeader } from "../../../common/components/CommonPlayerAndTeamComponent/DetailsTeamAdnPlayersComponent";
import { CommonHeaderName } from "../../../common/components/CommonPlayerAndTeamComponent/DetailsTeamAdnPlayersComponent";
// styles
import "../../../ui/commonStyleClasses/commonStyle.css";
import "./DetailTeam.css";
// redux
import {deleteTeam, changeUpdateTeamStatus} from '../../../core/redux/teams/teamCreateSlice'

const DetailTeam = () => {
  // const [dataTeam, setDataTeam] = useState({}) 

  // const selectorTeamsData = useSelector(state => state.teams.teams)
  // const params = useParams()
  // console.log(params);
  
  // const data = selectorTeamsData.data.map(el => {
  //   console.log(el.id, 'id');
    
  //   if(Number(params.id) === el.id) {
  //   const dataId = {
  //     name: el.name,
  //     division: el.division,
  //     imageUrl: el.imageUrl,
  //     conference: el.conference,
  //     foundationYear: el.foundationYear
  //   }   
  //   return dataId   

  //   }
    
  // })
  // const {name, division, imageUrl, conference, foundationYear} = data[0]
  // console.log(data[0])


  const dispatch = useDispatch()
  const selectorGetTeam = useSelector(state => state.teams.getTeam)
  const redirect = useNavigate()
  
  const {id, name, foundationYear, division, imageUrl, conference} = selectorGetTeam

  // const updateTeamData = {
    
  //   name,
  //   foundationYear,
  //   division,
  //   conference,
  //   imageUrl,
  //   id
  // }

  const changeStatus = () => {
    dispatch(changeUpdateTeamStatus(true))
    redirect('/addteam')
    console.log('updatedstatus');
    
  }

  const handleDelete = () => {
    dispatch(deleteTeam(id))
    dispatch(changeUpdateTeamStatus(false))    
    redirect('/addteam')
  }
  
  return (
    <section className="detail-team container">
      <CommonlHeader text="team" />
      <main>
        <div className="card-team">
          <CommonHeaderName 
          changeStatus={changeStatus}
          handleDelete={handleDelete}
          title="Teams" name="Denver Nuggets" />
          <div className="card-team__profile">
            <div className="profile__logo"></div>
            <span className="profile__name common-text-style">
              {name}
            </span>
            <div className="profile__data">
              <span className="data__text-level-top common-text-style">
                Year of foundation
              </span>
              <span className="data__text-level-down common-text-style">
                {foundationYear}
              </span>
            </div>
            <div className="profile__data conference">
              <span className="data__text-level-top common-text-style">
                Conference
              </span>
              <span className="data__text-level-down common-text-style">
                {conference}
              </span>
            </div>
            <div className="profile__data division">
              <span className="data__text-level-top common-text-style">
                Division
              </span>
              <span className="data__text-level-down common-text-style">
                {division}
              </span>
            </div>
          </div>
        </div>
        <div className="line-grey"></div>
        <div className="detail-team__list-players">
          <div className="list-players__header">
            <div className="header__roster">
              <span className="roster common-text-style">Roster</span>
            </div>
            <div className="header__player">
              <span className="player__lattice common-text-style">#</span>
              <span className="player__text common-text-style">Player</span>
            </div>
          </div>

          <div className="list-players__players">
            <div className="players__player">
              <span className="player__number common-text-style">10</span>
              <div className="player__image"></div>
              <div className="player__description">
                <span className="description__name">Bol Bol</span>
                <span className="description__position">Centerforward</span>
              </div>
            </div>
          </div>
        </div>
      </main>
    </section>
  );
};

export default DetailTeam;
