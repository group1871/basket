import React, { useEffect } from "react";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
// components
import Header from "../../common/components/PlayersHeader";
import { InputSearch } from "../../common/components/CommonPlayerAndTeamComponent/PlayerAndTeamComponent";
import { Button } from "../../common/components/CommonPlayerAndTeamComponent/PlayerAndTeamComponent";
import { EmptyContainer } from "../../common/components/CommonPlayerAndTeamComponent/PlayerAndTeamComponent";
import { PaginationContainer } from "../../common/components/CommonPlayerAndTeamComponent/PlayerAndTeamComponent";
// styles
import { SelectPaginationStyles } from "../../ui/styledComponents/Multiselects/SelectPaginationStyles";
// redux
import { getTeams, searchTeam, changeAddTeamStatus, changeCountSelectPaginate } from "../../core/redux/teams/teamCreateSlice";

const optionsPaginate = [
  { label: "6", value: "6" },
  { label: "12", value: "12" },
  { label: "24", value: "24" },
];

const options = [
  { label: "angular", value: "angular js" },
  { label: "react", value: "react js" },
];

const Teams = () => {
  //const [addPlayer, setAddPlayer] = useState(false)
  const redirect = useNavigate();
  const dispatch = useDispatch();

  const selectorTeams = useSelector((state) => state.teams.teams);
  const selectorCountSelectPaginate = useSelector(state => state.teams.countSelectPaginate)

  const {name, count} = selectorTeams
 
  const changeCountSelect = (value) => {
    dispatch(changeCountSelectPaginate(value))
  }
  

  const selectorAddTeamStatus = useSelector(
    (state) => state.teams.isAddTeamStatus
  );

  function addNewPlayer() {
    //setAddPlayer(true)
    redirect("/addteam");
  }

  useEffect(() => {

   
       dispatch(getTeams());

      if(count >= 1) {
       dispatch(changeAddTeamStatus(true))
    }
     
   
  }, []);


  const handleSearchTeam = (event) => {
    dispatch(searchTeam(event.target.value))
    
  }

 
  return (
    <section className="players container">
      <header>
        <h2 className="players__h2">Teams</h2>
        <Header />
      </header>
      <main>
        <div style={{ height: "150px", background: "#F6F6F6" }}>
          <InputSearch 
            handleSearchTeam={handleSearchTeam}
          />
          <Button addNewPlayer={addNewPlayer} />
        </div>
        
        <EmptyContainer 
        selectorAddStatus={selectorAddTeamStatus}
        text="team" />
      </main>
      <footer>
        <PaginationContainer
          optionsPaginate={optionsPaginate}
          SelectPaginationStyles={SelectPaginationStyles}
          defaultValue={optionsPaginate[0]}
          changeCountSelect={changeCountSelect}
          selectorCountSelectPaginate={selectorCountSelectPaginate}
/>
      </footer>
    </section>
  );
};

export default Teams;
