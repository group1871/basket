import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router";
import { useDispatch, useSelector } from "react-redux";
// components
import {
  CommonHeader,
  CommonTitle,
  CommonInputImage,
  CommonInput,
  CommonButton,
} from "../../../common/components/CommonPlayerAndTeamComponent/AddTeamAndAddPlayerComponent";
// styles
import "../../../ui/commonStyleClasses/commonStyle.css";
import "./AddTeam.css";
// redux
import {
  addNewTeam,
  updateTeam,
  changeAddTeamStatus
  
} from "../../../core/redux/teams/teamCreateSlice";
import { addImages } from "../../../core/redux/images/imagesCreateSlice";

const AddTeam = () => {
  const { register, handleSubmit, reset, getValues } = useForm();

  const [teamData, setTeamData] = useState(null);
  const selectorGetTeam = useSelector(state => state.teams.getTeam)
  const selectorIsUpdateTeamStatus = useSelector(state => state.teams.isUpdateTeamStatus)
  // const selectorGetImage = useSelector(state => state.images.images)
  // const selectorTeamsSlice = useSelector(state => state.teams.teamsSlice)

  const [count, setCount] = useState(1)
  //const valueImageUrl = getValues('imageUrl')
  //console.log(valueImageUrl[0].name);
  
  
const {id, name, division, foundationYear, imageUrl, conference} = selectorGetTeam

  const redirect = useNavigate();
  const dispatch = useDispatch()

  const cancelAddDataPlayer = () => {
    setTeamData("");
    reset({
      name: "",
      width: "",
      height: "",
      birthday: "",
      avatar: "",
      number: "",
    });
    redirect("/teams");
  };


  const onSubmit = (data) => {
    
    //const valueImageUrl = getValues('imageUrl')
    const formData = new FormData()
    formData.append('imageUrl', data.imageUrl[0]);
    console.log(data.imageUrl[0]);
    //const image  
    
    const dataTeam = {
      name: data.name,
      division: data.division,
      conference: data.conference,
      foundationYear: Number(data.foundationYear),
      imageUrl: data.imageUrl[0].name,
      id: id
    };
    // const dataTeam = {
    //   name: 'name',
    //   division: 'division',
    //   conference: 'conference',
    //   foundationYear: 1970,
    //   imageUrl: data.imageUrl[0].name,
    //   id: id
    // };
    //const image = formData.get('photoTeam')
    //console.log(image);
    

    if(selectorIsUpdateTeamStatus) {
      dispatch(updateTeam(dataTeam));
      redirect(`/detailteam${id}`);
 
    } else {
      dispatch(addNewTeam(dataTeam));
      dispatch(addImages(formData.get('imageUrl')))
      redirect("/teams");
      
    }

    dispatch(changeAddTeamStatus(true));

  };

  return (
    <section className="addteam container">
      <header>
        <CommonHeader headerTitle="AddTeam" />
      </header>
      <main className="addteam__main">
        
        <CommonTitle title="Teams" name="Add new team" />
       
        <CommonInputImage 
        register={register} name="imageUrl"
        selectorisUpdateStatus={selectorIsUpdateTeamStatus}
        valueImage={imageUrl}/>
        <CommonInput register={register} label="Name"
        selectorisUpdateStatus={selectorIsUpdateTeamStatus}
        name="name" value={name}/>

        <CommonInput
          register={register}
          label="Division"
          name="division"
          selectorisUpdateStatus={selectorIsUpdateTeamStatus}
          value={division}
        />

        <CommonInput
          register={register}
          label="Conference"
          name="conference"
          value={conference}
        />

        <CommonInput
          register={register}
          label="Year of foundation"
          name="foundationYear"
          value={foundationYear}
        />
        <CommonButton
          cancelAddDataPlayer={cancelAddDataPlayer}
          handleSubmit={handleSubmit}
          onSubmit={onSubmit}
        />
      </main>
    </section>
  );
};

export default AddTeam;
