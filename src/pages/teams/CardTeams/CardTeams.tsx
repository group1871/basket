import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router";
// styles
import "./CardTeams.css";
import "../../../ui/commonStyleClasses/commonStyle.css";
// redux
import { getTeam } from "../../../core/redux/teams/teamCreateSlice";

const CardTeams = () => {
  const dispatch = useDispatch()
  //const selectorTeamsImage = useSelector((state) => state.teams.teams);
  const selectorSearch = useSelector(state => state.teams.search)
  const selectorTeamsSlice = useSelector(state => state.teams.teamsSlice)

  const redirect = useNavigate()
    
  const cards = selectorTeamsSlice.map(item => {
    const redirectToDetailTeam = () => {
       dispatch(getTeam(item.id))
      redirect(`/detailteam${item.id}`)
    } 
     if(item.name.includes(selectorSearch)) {
      return (
        <div 
        onClick={redirectToDetailTeam}
        key={item.id} className="card-teams__image-container">
          {/* <img src="../../../assets/images/player.png" /> */}
          <div className="image-container__image"></div>
            <div className="image__down-card">
              <span className="down-card__name">{item.name}</span>
              <span className="down-card__year">{item.foundationYear}</span>
            </div>
        </div>
      )
      }
  })
  
    
  
  return (
    <div className="card-teams">
        {cards}
     </div>
      
  )
  
}
export default CardTeams;
