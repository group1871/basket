export const multiSelectStyles = {
  multiValueLabel: (provided, state) => ({
    ...provided,
    color: "white",
    width: 142,
    height: 24,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: 4,
    gap: 8,
    background: "#E4163A",
    borderRadius: 4,
  }),
  multiValueRemove: (provided, state) => ({
    ...provided,
    background: "#E4163A",
    color: "white",

    flex: "none",
    order: 1,
    flexGrow: 0,
    after: "content",
  }),
  singleValue: (provided, state) => ({
    ...provided,
    backgroundColor: "red",
    color: "white",
  }),
  input: (provided, state) => ({
    ...provided,

    height: 40,
  }),

  placeholder: (provided, state) => ({
    ...provided,
    fontFamily: "Avenir",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: 17,
    color: "#707070",
    paddingLeft: 5,
  }),
};

export const stylesAddPlayer = {
  multiValueLabel: (provided, state) => ({
    ...provided,
    fontFamily: "Avenir",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: 14,
    lineHeight: 24,
    color: "#303030",
    width: 101,
    height: 24,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
  }),
  multiValue: (provided, state) => ({
    ...provided,
    backgroundColor: "none",
  }),

  multiValueRemove: (provided, state) => ({
    ...provided,
    display: "none",
  }),
  singleValue: (provided, state) => ({
    ...provided,
    backgroundColor: "red",
    color: "white",
  }),

  control: (provided, state) => ({
    ...provided,
    background: "#F6F6F6",
    height: 40,
  }),

  placeholder: (provided, state) => ({
    ...provided,
    fontFamily: "Avenir",
    fontStyle: "normal",
    fontWeight: 500,
    fontSize: 17,
    color: "#707070",
    paddingLeft: 5,
  }),
};
