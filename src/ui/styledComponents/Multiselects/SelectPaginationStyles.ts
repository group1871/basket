export const SelectPaginationStyles = {
  dropdownIndicator: (provided, state) => ({
    ...provided,

    padding: 0,
  }),
  control: (provided, state) => ({
    ...provided,
    minWidth: 60,
    minHeight: 28,
  }),
};
