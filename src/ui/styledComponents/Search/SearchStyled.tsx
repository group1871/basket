import styled from "styled-components";

export const Search = styled.input`
  width: 364px;
  height: 40px;

  border-radius: 4px;
  border: 0.5px solid #d1d1d1;

  box-sizing: border-box;

  background: #ffffff;
`;
