import styled from "styled-components";

export const InputStyle = styled.input`
  width: 327px;
  height: 40px;
  border: none;
  outline: none;
  background: #f6f6f6;
  border-radius: 4px;

  font-family: "Avenir";
  font-style: normal;
  font-weight: 500;

  color: #707070;

  font-size: 14px;

  line-height: 24px;

  color: #303030;

  &:hover {
    background: #d1d1d1;
  }

  &:focus {
    background: #f6f6f6;

    box-shadow: 0px 0px 5px #d9d9d9;
  }
`;

export const InputStyleWidthSmall = styled(InputStyle)`
  width: 151.5px;
  font-size: 15px;
  line-height: 24px;
  color: #303030;
`;


