import styled from "styled-components";

export const Button = styled.button`
  width: 327px;
  height: 40px;

  background: #e4163a;
  border-radius: 4px;
  color: white;
  border-color: red;

  font-size: 15px;
  line-height: 24px;

  margin-top: 24px;
  cursor: pointer;

  &:hover {
    background: #ff5761;
    cursor: pointer;
  }

  &:active {
    background: #c60e2e;
    cursor: pointer;
  }
`;

export const ButtonStyled = styled.button`
  width: 351px;
  height: 40px;

  background: #e4163a;
  border-radius: 4px;
  color: white;
  border-color: red;

  font-size: 15px;
  line-height: 24px;

  margin-top: 24px;
  cursor: pointer;
`;
export const ButtonSmallStyleCancel = styled(Button)`
  width: 159px;
  height: 40px;

  font-size: 15px;

  line-height: 24px;

  color: #9c9c9c;

  background: white;

  border: 1px solid #9c9c9c;
`;

export const ButtonSmallStyleSave = styled(Button)`
  width: 159px;
  height: 40px;

  font-size: 15px;

  line-height: 24px;

  color: #ffffff;

  background: #e4163a; ;
`;
