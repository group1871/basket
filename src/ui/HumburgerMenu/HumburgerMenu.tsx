import React, { useState } from "react";
import classNames from "classnames";
import { useNavigate } from "react-router";
// styles
import "./HumburgerMenu.css";
import "../../ui/commonStyleClasses/commonStyle.css";
// helpers

const HumburgerMenu = () => {
  const localStorage = window.localStorage;

  const [visibleShadow, setVisibleShadow] = useState(false);

  let [authSucses, setAuthSucses] = useState("true");

  const redirect = useNavigate();

  const setVisibleBackShadowMenu = () => {
    setVisibleShadow(!visibleShadow);
  };
  localStorage.setItem("authSucses", authSucses);

  const exitPersonalOffice = () => {
    setAuthSucses((authSucses = "false"));

     redirect("/");
     window.location.reload();
  };

  const redirectToPlayers = () => {
    redirect("/players");
  };

  const redirectToTeams = () => {
    redirect("/teams");
  };

  return (
    <div className="hamburger-menu">
      <div className={classNames("visible", { shadow: visibleShadow })}></div>
      <input id="menu__toggle" type="checkbox" />
      <label
        onClick={setVisibleBackShadowMenu}
        className="menu__btn"
        htmlFor="menu__toggle"
      >
        <span></span>
      </label>

      <div className="menu__box">
        <div className="profile">
          <div className="profile__header">
            <div className="profile__header-image"></div>
            <span className="profile__header-text">John Smith</span>
          </div>
        </div>

        <div className="menu-box__menu-body">
          <div className="menu-body__wrapper">
            <div className="wrapper__image image-group-person"></div>
            <span
              onClick={redirectToTeams}
              className="wrapper__text text-teams"
            >
              Teams
            </span>
          </div>

          <div className="menu-body__wrapper">
            <div className="wrapper__image image-person"></div>
            <span
              onClick={redirectToPlayers}
              className="wrapper__text text-players"
            >
              Players
            </span>
          </div>
        </div>

        <div className="menu-box__menu-footer">
          <div className="menu-footer__wrapper" onClick={exitPersonalOffice}>
            <div className="wrapper__image image-footer"></div>
            <span className="wrapper__text text-sign-out">Sign out</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HumburgerMenu;
