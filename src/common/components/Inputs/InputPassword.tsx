import React from "react";
import classNames from "classnames";
// styles
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";
// hooks
import { useOpenAndCloseEyePassword } from "../../../common/components/hooks/useOpenAndCloseEyePassword";

const InputPassword = ({ formsProps, required, password }) => {
  const { register, maxLength, formState } = formsProps;

  const { changeStateCloseToOpenAndReverse, isOpenEye } =
    useOpenAndCloseEyePassword();

  return (
    <div className="form__wrapper-input-password wrapper-input">
      <label htmlFor="password" className="common-text-style">
        Password
      </label>
      <InputStyle
        type={isOpenEye ? "text" : "password"}
        {...register(password, { required, maxLength })}
      />
      <div className="errors-forms">
        {formState.errors.password?.type === "required" &&
          "поле пароль обязательно"}
        {formState.errors.password?.type === "maxLength" &&
          "длина пароля не более 10 символов"}
      </div>

      <div
        onClick={changeStateCloseToOpenAndReverse}
        className={classNames("close-eye", { "open-eye": isOpenEye })}
      ></div>
    </div>
  );
};

export default InputPassword;
