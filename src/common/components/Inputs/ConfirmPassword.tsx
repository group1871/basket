import React, { useState } from "react";
import { useForm } from "react-hook-form";
import classNames from "classnames";
// styles
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";
// hooks
import { useOpenAndCloseEyePassword } from "../../../common/components/hooks/useOpenAndCloseEyePassword";

const ConfirmPassword = () => {
  const { changeStateCloseToOpenAndReverse, isOpenEye } =
    useOpenAndCloseEyePassword();

  const [isOpenEyePasswordConfirm, setIsOpenEyePasswordConfirm] =
    useState(false);

  const changeFromCloseToOpenPassConf = () => {
    setIsOpenEyePasswordConfirm(!isOpenEyePasswordConfirm);
  };

  const {
    register,
    formState: { errors },
  } = useForm();

  return (
    <>
      <div className="form__wrapper-input-password wrapper-input">
        <label htmlFor="password" className="common-text-style">
          Password
        </label>
        <InputStyle
          type={isOpenEye ? "text" : "password"}
          {...register("password", { required: true, maxLength: 20 })}
        />
        <div className="errors-forms">
          {errors.password?.type === "required" && "поле пароль обязательно"}
          {errors.password?.type === "maxLength" &&
            "длина пароля не более 10 символов"}
        </div>

        <div
          onClick={changeStateCloseToOpenAndReverse}
          className={classNames("close-eye", { "open-eye": isOpenEye })}
        ></div>
      </div>

      <div className="form__wrapper-input-confirm wrapper-input">
        <label htmlFor="password-confirm" className="common-text-style">
          Enter your password again
        </label>
        <InputStyle
          type={isOpenEye ? "text" : "password"}
          {...register("passwordConfirm", { required: true, maxLength: 20 })}
        />
        <div className="errors-forms">
          {errors.passwordConfirm?.type === "required" &&
            "подтверждение пароля обязательно"}
        </div>
        <div
          onClick={changeFromCloseToOpenPassConf}
          className={classNames("close-eye", {
            "open-eye": isOpenEyePasswordConfirm,
          })}
        ></div>
      </div>
    </>
  );
};

export default ConfirmPassword;
