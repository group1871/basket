import React, { useState } from "react";
import classNames from "classnames";
// styles
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";
// hooks
import { useOpenAndCloseEyePassword } from "../../../common/components/hooks/useOpenAndCloseEyePassword";

const InputPasswordConfirm = ({ formsProps, required, passwordConfirm }) => {
  const { register, maxLength, formState } = formsProps;

  const [isOpenEyePasswordConfirm, setIsOpenEyePasswordConfirm] =
    useState(false);

  const { isOpenEye } = useOpenAndCloseEyePassword();

  const changeFromCloseToOpenPassConf = () => {
    setIsOpenEyePasswordConfirm(!isOpenEyePasswordConfirm);
  };

  return (
    <div className="form__wrapper-input-confirm wrapper-input">
      <label htmlFor="password-confirm" className="common-text-style">
        Enter your password again
      </label>
      <InputStyle
        type={isOpenEye ? "text" : "password"}
        {...register(passwordConfirm, { required, maxLength })}
      />
      <div className="errors-forms">
        {formState.errors.passwordConfirm?.type === "required" &&
          "подтверждение пароля обязательно"}
      </div>
      <div
        onClick={changeFromCloseToOpenPassConf}
        className={classNames("close-eye", {
          "open-eye": isOpenEyePasswordConfirm,
        })}
      ></div>
    </div>
  );
};

export default InputPasswordConfirm;
