import React from "react";
// styles
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";

const InputLogin = ({ formsProps, required, login }) => {
  const { register, maxLength, formState } = formsProps;

  return (
    <div className="form__wrapper-input-login wrapper-input">
      <label htmlFor="login" className="common-text-style">
        Login
      </label>
      <InputStyle {...register(login, { required, maxLength })} />
      <div className="errors-forms">
        {formState.errors.login?.type === "required" &&
          "поле логин обязательно"}
        {formState.errors.login?.type === "maxLength" &&
          "длина логина не более 20 символов"}
      </div>
    </div>
  );
};

export default InputLogin;
