import React from "react";
// styles
import "../../../pages/Forms/Registration/Registration.css";

const InputCheckbox = ({ formsProps, required, checkboxRuleChecked }) => {
  const { register, formState } = formsProps;

  return (
    <>
      <div className="form__wrapper-input-checkbox wrapper-input">
        <input
          type="checkbox"
          className="wrapper-input__checkbox"
          {...register(checkboxRuleChecked, { required })}
        />

        <label htmlFor="checkbox" className="wrapper-input__checkbox-inline">
          I accept the agreement
        </label>
      </div>
      <div className="errors-forms error-checked">
        {formState.errors.checkboxRuleChecked?.type === "required" &&
          "отметьте чекбокс"}
      </div>
    </>
  );
};

export default InputCheckbox;
