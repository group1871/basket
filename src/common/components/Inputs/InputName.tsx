import React from "react";
import classNames from "classnames";
// styles
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";
import "../../../ui/commonStyleClasses/commonStyle.css";
import "../../../pages/Forms/Registration/Registration.css";

const InputName = ({ formsProps, required, name }) => {
  const { register, maxLength, formState } = formsProps;

  return (
    <div className="form__wrapper-input-name wrapper-input">
      <label htmlFor="name" className="common-text-style">
        Name
      </label>
      <InputStyle
        className={classNames({ inv: formState.isValid })}
        {...register(name, { required, maxLength })}
      />

      <div className="errors-forms">
        {formState.errors.name?.type === "required" && "поле имя обязательно"}
        {formState.errors.name?.type === "maxLength" &&
          "длина имени не более 20 символов"}
      </div>
    </div>
  );
};

export default InputName;
