import React from "react";
import HumburgerMenu from "../../ui/HumburgerMenu/HumburgerMenu";

const Header = () => {
  return (
    <div className="players__header">
      <HumburgerMenu />
      <div className="header__icon-navigation-menu"></div>
      <div className="header__icon-test"></div>
    </div>
  );
};

export default Header;
