export const recordAndSaveDataToLocalStorageAndRedirect = (
  token,
  localStore,
  redirect
) => {
  if (token) {
    redirect("/teams");
    window.location.reload();
    return localStore;
  } else {
    return;
  }
};
