import React from "react";
import "../DengerNuggets/DengerNuggets.css";

const DengerNuggets = () => {
  return (
    <div className="card-team">
      <div className="card-team__wrapper">
        <div className="wrapper__header">
          <span className="wrapper__header-text">Teams / Denver Nuggets</span>
          <div className="wrapper-svg">
            <div className="create"></div>
            <div className="delete"></div>
          </div>
        </div>
      </div>

      <div className="card-team__main">
        <div className="main__image"></div>

        <div className="card-team__text">
          <span className="text__denver common-style-text">
            {" "}
            Denver Nuggets{" "}
          </span>
          <div className="wrapper">
            <span className="text__year-foundation common-style-text">
              {" "}
              Year of foundation{" "}
            </span>
            <span className="text__division common-style-text"> Division </span>
          </div>

          <div className="wrapper2">
            <span className="text__year common-style-text"> 1976 </span>
            <span className="text__northwestern common-style-text">
              {" "}
              Northwestern{" "}
            </span>
          </div>

          <div className="wrapper3">
            <span className="text__conference common-style-text">
              {" "}
              Conference{" "}
            </span>
            <span className="text__western common-style-text"> Western </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DengerNuggets;
