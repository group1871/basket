import React, { useState } from "react";
// components
import { ButtonStyled } from "../../../ui/styledComponents/Button/ButtonStyled";
import PaginatedItems from "../Paginate/Paginate";
import Select from "react-select";
import CardTeams from "../../../pages/teams/CardTeams/CardTeams";
import CardPlayers from "../../../pages/ players/CardPlayers/CardPlayers";


interface PropEmptyContainer {
  text: string;
  selectorAddStatus: boolean,

}

export const EmptyContainer = (props: PropEmptyContainer) => {
  const {selectorAddStatus } = props
  
  if(!selectorAddStatus || !selectorAddStatus) {
    return (
      <div className="add-new-players">
        <div className="add-new-players__image-player"></div>
        <span className="add-new-players__text-empty common-text-style color-pink">
          Empty here
        </span>
        <span className="add-new-players__text-add-new-players common-text-style">
          Add new {props.text} to continue
        </span>
      </div>
    );
  } else {
    return <CardTeams /> || <CardPlayers />
  }
  
};

export const InputSearch = (props) => {
  const {handleSearchTeam} = props
  return (
    <div className="search__input-search">
      <input
        onChange={handleSearchTeam}
        className="input-search__input common-text-style"
        placeholder="Search..."
        type="text"
      />
      <div className="input-search__logo-search"></div>
    </div>
  );
};
interface PropsButton {
  addNewPlayer: Function;
}
export const Button = (props: PropsButton) => {  
  return (
    <div className="players__button">
      <ButtonStyled onClick={props.addNewPlayer}>Add +</ButtonStyled>
    </div>
  );
};

interface PropsPaginate {
  optionsPaginate: {}[];
  SelectPaginationStyles: object;
  defaultValue: any;
  changeCountSelect: Function
  selectorCountSelectPaginate:any
}
export const PaginationContainer = (props: PropsPaginate) => {

const {optionsPaginate, SelectPaginationStyles, 
  defaultValue, changeCountSelect, selectorCountSelectPaginate} = props

const [value, setValue] = useState(defaultValue)

  const handleChange = (value) => {
    setValue(value)
    console.log(value.value);
    changeCountSelect(value.value)
    
    
  }

  return (
    <div className="pagination-container">
      <div className="pagination-container__container">
        <PaginatedItems
          itemsPerPage={selectorCountSelectPaginate}
        />
        <div className="container__multiselect">
          <Select
            onChange={handleChange}
            styles={SelectPaginationStyles}
            menuPosition="fixed"
            options={optionsPaginate}
            defaultValue={defaultValue}
            value={value}
          />
        </div>
      </div>
    </div>
  );
};
