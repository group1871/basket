import React from "react";
import Header from "../PlayersHeader";

export const CommonlHeader = (props) => {
  const { text } = props.text;
  return (
    <header>
      <h2 className="detail-player__h2">Detail {text}</h2>
      <Header />
      <div className="line-grey"></div>
    </header>
  );
};

export const CommonHeaderName = (props) => {
  const { title, name, handleDelete, changeStatus } = props;
  return (
    <div className="card-player__player-name">
      <div className="player-title common-text-style">
        {title} / {name}
      </div>
      <div className="player-logo">
        <div 
        onClick={changeStatus}
        className="player-logo__logo-create"></div>
        <div 
        onClick={handleDelete}
        className="player-logo__logo-delete"></div>
      </div>
    </div>
  );
};
