// components
import Header from "../PlayersHeader";
import { InputStyle } from "../../../ui/styledComponents/inputs/InputStyled";
import {
  ButtonSmallStyleCancel,
  ButtonSmallStyleSave,
} from "../../../ui/styledComponents/Button/ButtonStyled";
// styles
import "../../../ui/commonStyleClasses/commonStyle.css";

interface Header {
  headerTitle: string;
}

export const CommonHeader = (props: Header) => {
  const { headerTitle } = props;
  return (
    <div className="addplayer__header">
      <h2 className="header__h2">{headerTitle}</h2>
      <Header />
    </div>
  );
};

interface PlayerName {
  title: string;
  name: string;
}

export const CommonTitle = (props: PlayerName) => {
  const { title, name } = props;
  return (
    <div className="addplayer-container__player-name">
      <div className="player-title common-text-style">
        {title} / {name}
      </div>
    </div>
  );
};

export const CommonInputImage = (props) => {
  const { register, name, valueImage, selectorisUpdateStatus } = props;
  const valid = selectorisUpdateStatus ? valueImage : null
  return (
    <div className="profile__add-image">
      <input
     
        {...register(name, {value: valid, required: true })}
        accept="image/*"
        type="file"
        className="add-image"
      />

      <div className="profile__shadow"></div>
      <div className="profile__photo"></div>
    </div>
  );
};

export const CommonInput = (props) => {
  const { register, label, name, value, selectorisUpdateStatus } = props;
  const valid = selectorisUpdateStatus ? value : null

  return (
    <div className="common-input">
      <label htmlFor={label} className="common-text-style">
        {label}
      </label>
      <InputStyle
        {...register(name, { value: valid, maxLength: 20 })}
        id="name"
        className="input-name"
        placeholder="Greg Whittington"
      />
    </div>
  );
};

interface Button {
  cancelAddDataPlayer: Function;
  handleSubmit: Function;
  onSubmit: Function;
}

export const CommonButton = (props: Button) => {
  const { cancelAddDataPlayer, handleSubmit, onSubmit } = props;
  return (
    <div className="profile__button common-input-data">
      <div className="common-margin-left">
        <ButtonSmallStyleCancel onClick={cancelAddDataPlayer}>
          Cancel
        </ButtonSmallStyleCancel>
      </div>
      <div className="common-margin-left">
        <ButtonSmallStyleSave 
        onClick={handleSubmit(onSubmit)}>
          Save
        </ButtonSmallStyleSave>
        {/* <input type="submit" /> */}
      </div>
    </div>
  );
};
