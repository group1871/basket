import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import { useSelector, useDispatch } from "react-redux";
// components
import CardPlayers from "../../../pages/ players/CardPlayers/CardPlayers";
import CardTeams from "../../../pages/teams/CardTeams/CardTeams";
// styles
import "./Paginate.css";
import "../../../ui/commonStyleClasses/commonStyle.css";
// redux
import {getTeams, sliceTeams, sliceFlag} from '../../../core/redux/teams/teamCreateSlice'

const items = [...Array(100).keys()];

function PaginatedItems({ itemsPerPage }) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);
  const dispatch = useDispatch()
  const selectorTeams = useSelector(state => state.teams.teams)
  const selectorStatus = useSelector(state => state.teams.status)
  const selectorTeamsSlice = useSelector(state => state.teams.teamsSlice)

  

  
  useEffect(() => {
    
   
    switch (selectorStatus) {
      case 'resolved':
        const {data} = selectorTeams
          const endOffset = itemOffset + itemsPerPage;
          console.log(itemsPerPage, 'itemsPerPage');
          
          setCurrentItems(data.slice(itemOffset, endOffset))

        console.log(`Loading items from ${itemOffset} to ${endOffset}`);
        setPageCount(Math.ceil(items.length / itemsPerPage));
        //dispatch(sliceFlag(true))
        console.log(true);
        console.log(selectorTeamsSlice,'selectorTeamsSlice');


        break;
    
      default:
        break;
    
    }
   

  },[selectorStatus, itemOffset, itemsPerPage]);
 

  useEffect(() => {
    if(currentItems !== null) {
      dispatch(sliceTeams(currentItems))
    }
     
    
  }, [currentItems])
 
  
  console.log(currentItems);
  
  

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % items.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);

  };

  return (
    <>
      <ReactPaginate
        className="cont"
        nextLabel=">"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        pageCount={27}
        previousLabel="<"
        pageClassName="page-item common-text-style"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        // breakLabel="..."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
        renderOnZeroPageCount={null}
      />
    </>
  );
}

export default PaginatedItems;
