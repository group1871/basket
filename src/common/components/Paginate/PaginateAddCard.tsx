import React, { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
// import {useSelector} from 'react-redux'
// components
import CardPlayers from "../../../pages/ players/CardPlayers/CardPlayers";
import CardTeams from "../../../pages/teams/CardTeams/CardTeams";
// styles
import "./Paginate.css";
import "../../../ui/commonStyleClasses/commonStyle.css";

// const selectorTeamsImage = useSelector(state => state.teams.teams)

// const {imageUrl} = selectorTeamsImage

const items = [...Array(100).keys()];
// console.log(selectorTeamsImage, 'selectorTeamsImage');

function PaginatedAddCard({ itemsPerPage }) {
  const [currentItems, setCurrentItems] = useState(null);
  const [pageCount, setPageCount] = useState(0);
  const [itemOffset, setItemOffset] = useState(0);

  useEffect(() => {
    const endOffset = itemOffset + itemsPerPage;
    console.log(`Loading items from ${itemOffset} to ${endOffset}`);
    setCurrentItems(items.slice(itemOffset, 6));
    setPageCount(Math.ceil(items.length / itemsPerPage));
  }, [itemOffset, itemsPerPage]);

  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % items.length;
    console.log(
      `User requested page number ${event.selected}, which is offset ${newOffset}`
    );
    setItemOffset(newOffset);
  };

  return (
    <>
      {/* <CardTeams currentItems={currentItems} /> */}
      <ReactPaginate
        className="cont"
        nextLabel=">"
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        marginPagesDisplayed={1}
        pageCount={27}
        previousLabel="<"
        pageClassName="page-item common-text-style"
        pageLinkClassName="page-link"
        previousClassName="page-item"
        previousLinkClassName="page-link"
        nextClassName="page-item"
        nextLinkClassName="page-link"
        // breakLabel="..."
        breakClassName="page-item"
        breakLinkClassName="page-link"
        containerClassName="pagination"
        activeClassName="active"
        renderOnZeroPageCount={null}
      />
    </>
  );
}

export default PaginatedAddCard;
