export interface InputsForms {
  name: string;
  login: string;
  password: number;
  passwordConfirm: number;
  checkboxRuleChecked: boolean;
}

export interface IRequestBaseBody {
  userName: string;
  login: string;
  password: string;
  body: string;
}

export interface InputsAddPlayer {
  name: string;
  width: string;
  height: string;
  birthday: string;
  avatar: string;
  number: string;
}
