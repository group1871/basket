import React, { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router";
import { useDispatch } from "react-redux";

// components
import InputName from "../Inputs/InputName";
import InputLogin from "../Inputs/InputLogin";
import InputPassword from "../Inputs/InputPassword";
import InputPasswordConfirm from "../Inputs/InputPasswordConfirm";
import InputCheckbox from "../Inputs/InputCheckbox";
// styles
import { Button } from "../../../ui/styledComponents/Button/ButtonStyled";
// request
import { post } from "../../../api/request";
// interface
import { InputsForms } from "../interfaces/intarfaces";
// redux
import { fetchSignUp } from "../../../core/redux/auth/authCreateSlice";

const FormRegistration = () => {
   const [count, setCount] = useState(1)

  const dispatch = useDispatch();
  const redirect = useNavigate();

  const [userData, setUserData] = useState(null);

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<InputsForms>();

  const onSubmit: SubmitHandler<InputsForms> = (data) => {
     setCount(count + 1)
    setUserData({
      userName: data.name,
      login: data.login,
      password: data.password,
    });

    dispatch(fetchSignUp(userData));
    // dispatch(signUp(userData))
    if(count == 3) {
      redirect('/')

    }
  };


  const formsProps = {
    register,
    maxLength: 20,
    formState: { errors, isValid },
  };

  return (
    <>
      <form className="form">
        <InputName formsProps={formsProps} required name="name" />

        <InputLogin formsProps={formsProps} required login="login" />

        <InputPassword formsProps={formsProps} required password="password" />
        <InputPasswordConfirm
          formsProps={formsProps}
          required
          passwordConfirm="passwordConfirm"
        />

        <InputCheckbox
          formsProps={formsProps}
          required
          checkboxRuleChecked="checkboxRuleChecked"
        />

        <div className="form__button wrapper-input common-text-style">
          <Button
            onClick={handleSubmit(onSubmit)}
            className="button button_active"
            type="submit"
          >
            Sign Up
          </Button>
        </div>
      </form>
    </>
  );
};
export default FormRegistration;
