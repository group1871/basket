import React, { useState } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { useNavigate } from "react-router";
import { useSelector, useDispatch } from "react-redux";
// components
import InputLogin from "../Inputs/InputLogin";
import InputPassword from "../Inputs/InputPassword";
// styles
import { Button } from "../../../ui/styledComponents/Button/ButtonStyled";
// request
import { post } from "../../../api/request";
// interface
import { InputsForms } from "../interfaces/intarfaces";
// redux
import { fetchSignIn } from "../../../core/redux/auth/authCreateSlice";
// helpers
import { recordAndSaveDataToLocalStorageAndRedirect } from "../helpers/booleanOperations";

const localStorage = window.localStorage;

const FormAutorization = () => {
  const [userData, setUserData] = useState(null);
  let [authSucses, setAuthSucses] = useState("false");

  //const tokenFromRegistration = localStorage.getItem('token')
  const dispatch = useDispatch();
  const authData = useSelector((state) => state.auth.registrationData);

  console.log(authData, "reg");

  const { token } = authData;

  const redirect = useNavigate();

  const {
    register,
    handleSubmit,
    formState: { errors, isValid },
  } = useForm<InputsForms>();

  const onSubmit: SubmitHandler<InputsForms> = (data) => {
    setUserData({ login: data.login, password: data.password });
    setAuthSucses((authSucses = "true"));

    dispatch(fetchSignIn(userData));

    recordAndSaveDataToLocalStorageAndRedirect(
      token,
      localStorage.setItem("authSucses", authSucses),
      redirect
    );
  };

  const formsProps = {
    register,
    maxLength: 20,
    formState: { errors, isValid },
  };

  return (
    <>
      <form className="form">
        <InputLogin formsProps={formsProps} required login="login" />

        <InputPassword formsProps={formsProps} required password="password" />

        <div className="form__button wrapper-input common-text-style">
          <Button
            onClick={handleSubmit(onSubmit)}
            className="button button_active"
            type="submit"
          >
            Sign In
          </Button>
        </div>
      </form>
    </>
  );
};

export default FormAutorization;
