import { useState } from "react";

export const useOpenAndCloseEyePassword = () => {
  const [isOpenEye, setIsOpenEye] = useState(false);

  function changeStateCloseToOpenAndReverse() {
    setIsOpenEye(!isOpenEye);
  }

  return { changeStateCloseToOpenAndReverse, isOpenEye };
};
