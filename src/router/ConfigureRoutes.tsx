import { BrowserRouter, Routes, Route } from "react-router-dom";
// Components
import Registration from "../pages/Forms/Registration/Registration";
import Autorization from "../pages/Forms/Autorization/Autorization";

import AddPlayer from "../pages/ players/AddPlayer/AddPlayer";
import DetailPlayer from "../pages/ players/DetailPlayer/DetailPlayer";
import Players from "../pages/ players/Players/Players";

import AddTeam from "../pages/teams/AddTeam/AddTeam";
import CardTeams from "../pages/teams/CardTeams/CardTeams";
import DetailTeam from "../pages/teams/DetailTeam/DetailTeam";
import Teams from "../pages/teams/Teams";

import PageNotFound from "../pages/PageNotFound/PageNotFound";

const authSucses = localStorage.getItem("authSucses");

let authSucsesBoolean;

if (authSucses === "true") {
  authSucsesBoolean = true;
} else if (authSucses === "false") {
  authSucsesBoolean = false;
}

const ConfigureRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        {/* {!authSucsesBoolean ? (
          <> */}
            <Route path="/registration" element={<Registration />} />
            <Route index element={<Autorization />} />
          {/* </>
        ) : (
          <> */}
            <Route path="/addplayer" element={<AddPlayer />} />
            <Route path="/detailplayer" element={<DetailPlayer />} />
            <Route path="/players" element={<Players />} />

            <Route path="/addteam" element={<AddTeam />} />
            {/* <Route path="/cardteams" element={<CardTeams />} /> */}
            <Route path="/detailteam:id" element={<DetailTeam />} />
            <Route path="/teams" element={<Teams />} />
            {/* <Route path="" element={<div>not found</div>} /> */}
            <Route path="*" element={<PageNotFound />} />

          {/* </> */}
        )
      </Routes>
    </BrowserRouter>
  );
};

export default ConfigureRoutes;
