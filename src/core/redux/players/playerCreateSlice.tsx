import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// request
import { get, post, put, remove } from "../../../api/request";

const token = localStorage.getItem("token");

export const fetchPlayers:any = createAsyncThunk(
  "players/fetchPlayers",
  async function (_, { rejectWithValue }) {
    try {
      const response = await get(
        "Player/GetPlayers",
        token
      );
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deletePlayer:any = createAsyncThunk(
  "players/deletePlayer",
  async function (id, { rejectWithValue }) {
    try {
      const response = await remove(
        `Player/Delete/?=${id}`,
        token
      );
      console.log(response);
      return response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getPlayer:any = createAsyncThunk(
  "players/getPlayer",
  async function (id, { rejectWithValue }) {
    try {
      const response = await remove(
        `Player/Get/?=${id}`,
        token
      );
      console.log(response);
      return response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const updatePlayers:any = createAsyncThunk(
  "players/updatePlayer",
  async function (playerData, { rejectWithValue }) {
    const {id} = playerData

    try {
      const response = await put(
        `Player/Update/?=${id}`,
        JSON.stringify(playerData),
        token
      );
      console.log(response);
      return response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const addNewPlayer:any = createAsyncThunk(
  "players/addNewPlayer",
  async function (data, { rejectWithValue }) {
    try {
      const response = await post(
        "Player/Add",
        JSON.stringify(data),
        token
      );
      console.log(response);
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getPositions:any = createAsyncThunk(
  "players/getPositions",
  async function (_, { rejectWithValue }) {
    try {
      const response = await get(
        "Player/GetPositions",
        token
      );
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

// const setError = (state, action) => {
//     state.status = 'rejected';
//     state.error = action.payload;
// }

const playerSlice = createSlice({
  name: "players",
  initialState: {
    players: [],
    playersSlice: [],
    positions: [],
    status: null,
    error: null,
    isAddTeamStatus: false,
    isUpdatePlayerStatus: false,
    getPlayer: {},
    search: '',
    countSelectPaginate: 6


  },
  reducers: {
    changeAddPlayerStatus: (state, action) => {
      state.isAddTeamStatus = action.payload;
 
    },
    changeUpdatePlayerStatus: (state, action) => {
      state.isUpdatePlayerStatus = action.payload
    },
    searchPlayer: (state, action) => {
      state.search = action.payload    
      },
      slicePlayers: (state, action) => {
        state.playersSlice = action.payload
       
      },
      changeCountSelectPaginate: (state, action) => {
        state.countSelectPaginate = action.payload
      }

  },
  extraReducers: (builder) => {
    builder.addCase(fetchPlayers.fulfilled, (state, action) => {
      state.status = "resolved";
      state.players = action.payload;
    });
    builder.addCase(getPositions.fulfilled, (state, action) => {
      state.status = "resolved";
      state.positions = action.payload;
    });
    builder.addCase(getPlayer.fulfilled, (state, action) => {
      state.status = "resolved";
      state.getPlayer = action.payload;
    });
  },
});

export const {
  changeAddPlayerStatus, 
  changeUpdatePlayerStatus, 
  searchPlayer, 
  slicePlayers,
  changeCountSelectPaginate,
} = playerSlice.actions;

export default playerSlice.reducer;
