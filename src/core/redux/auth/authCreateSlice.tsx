import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// request
import { post } from "../../../api/request";

const token = localStorage.getItem("token");

export const fetchSignUp = createAsyncThunk(
  "SignUp/fetchSignUp",
  async (data, { rejectWithValue, dispatch }) => {
    try {
      await post(
        "Auth/SignUp",
        JSON.stringify(data)
      ).then((res) => localStorage.setItem("token", res.token));
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const fetchSignIn = createAsyncThunk(
  "SignIn/fetchSignIn",
  async (data, { rejectWithValue }) => {
    try {
      const response = await post(
        "Auth/SignIn",
        JSON.stringify(data),
        token
      );
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

const signUpSlice = createSlice({
  name: "SignUp",
  initialState: {
    registrationData: {},
  },
  reducers: {},

  extraReducers: (builder) => {
    builder.addCase(fetchSignIn.fulfilled, (state, action) => {
      state.registrationData = action.payload;
    });
  },
});

export default signUpSlice.reducer;
