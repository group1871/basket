import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
// request
import { post } from "../../../api/request";

const token = localStorage.getItem("token");

export const addImages = createAsyncThunk(
  "images/addImages",
  async function (image, { rejectWithValue }) {
    try {
      const response = await post(
        "Image/SaveImage",
        JSON.stringify(image),
        token
      );
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);
// const setError = (state, action) => {
//     state.status = 'rejected';
//     state.error = action.payload;
// }

const imagesSlice = createSlice({
  name: "images",
  initialState: {
    images: [],
    status: '',
   

  },
  reducers: {
  

  },
  extraReducers: (builder) => {
    builder.addCase(addImages.fulfilled, (state, action) => {
      state.status = "resolved";
      state.images = action.payload;
    });
   
  },
});

export const {} = imagesSlice.actions;

export default imagesSlice.reducer;
