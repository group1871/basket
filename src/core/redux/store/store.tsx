import { configureStore } from "@reduxjs/toolkit";
import authReducer from "../auth/authCreateSlice";
import playerReducer from "../players/playerCreateSlice";
import teamReducer from "../teams/teamCreateSlice";
import imageReducer from '../images/imagesCreateSlice';

const store =  configureStore({
  reducer: {
    auth: authReducer,
    players: playerReducer,
    teams: teamReducer,
    images: imageReducer,
  },
});

export type Dispatch = typeof store.dispatch

export default store