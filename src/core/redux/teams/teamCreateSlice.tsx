import { createSlice, createAsyncThunk, PayloadAction } from "@reduxjs/toolkit";
// request
import { get, post, put, remove } from "../../../api/request";

const token = localStorage.getItem("token");

export const getTeams:any = createAsyncThunk<object[]>(
  "teams/getTeams",
  async function (_, { rejectWithValue}) {
    try {
      const response = await get(
        "Team/GetTeams",
        token
      );
      return await response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const addNewTeam:any = createAsyncThunk(
  "teams/addNewTeam",
  async function (data, { rejectWithValue }) {
    try {
      const response = await post(
        "Team/Add",
        JSON.stringify(data),
        token
      );
      return response;
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const getTeam:any = createAsyncThunk(
  "teams/getTeam",
  async function (id, { rejectWithValue }) {
    try {
      const response = await get(
        `Team/Get/?id=${id}`,
        token
      );
      
      console.log(response);
      return response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const deleteTeam:any = createAsyncThunk(
  "teams/deleteTeam",
  async function (id, { rejectWithValue }) {
    try {
      const response = await remove(
        `Team/Delete/?id=${id}`,
        token
      );
      console.log(response);
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const updateTeam:any = createAsyncThunk(
  "teams/updateTeam",
  async function (teamData, { rejectWithValue }) {
    const { id } = teamData
    //console.log(id);
    
    try {
      const response = await put(
        `Team/Update/?id=${id}`,
        JSON.stringify(teamData),
        token
      );
      console.log(response);
      return response
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

// const setError = (state, action) => {
//     state.status = 'rejected';
//     state.error = action.payload;
// }

const teamSlice = createSlice({
  name: "teams",
  initialState: {
    teams: [],
    teamsSlice: [],
    getTeam: {},
    status: null,
    error: null,
    isAddTeamStatus: false,
    isUpdateTeamStatus: false,
    search: '',
    countSelectPaginate: 6
  },
  reducers: {
    changeAddTeamStatus: (state, action: PayloadAction<boolean>) => {
      state.isAddTeamStatus = action.payload;
    },
    changeUpdateTeamStatus: (state, action: PayloadAction<boolean>) => {
      state.isUpdateTeamStatus = action.payload
    },
    searchTeam: (state, action: PayloadAction<string>) => {
    state.search = action.payload    
    },
    sliceTeams: (state, action: PayloadAction<object[]>) => {
      state.teamsSlice = action.payload
     
    },
    changeCountSelectPaginate: (state, action: PayloadAction<number>) => {
      state.countSelectPaginate = action.payload
    }

  },
  extraReducers: (builder) => {
    builder.addCase(getTeams.fulfilled, (state, action: PayloadAction<object[]>) => {
      state.status = "resolved";
      state.teams = action.payload;
    });
    builder.addCase(getTeam.fulfilled, (state, action: PayloadAction<object[]>) => {
      state.status = "resolved";
      state.getTeam = action.payload;
    });
    builder.addCase(updateTeam.fulfilled, (state, action: PayloadAction<object[]>) => {
      state.status = "resolved";
      state.getTeam = action.payload;
    });
  },
});

export const { changeAddTeamStatus, 
  changeUpdateTeamStatus, searchTeam, sliceTeams, changeCountSelectPaginate } = teamSlice.actions;

export default teamSlice.reducer;
