import React from "react";
// styles
import "./App.css";
// router
import ConfigureRouter from "./router/ConfigureRoutes";

function App() {
  return (
    <div className="App">
      <ConfigureRouter />
    </div>
  );
}

export default App;
